
import UIKit

class Question {
    var question: String
    var currentAnswer: String
    var correctAnswer : String
    var isCorrect: Bool
    var variants: [String]
    
    init(question: String, currentAnswer: String, correctAnswer: String, isCorrect: Bool, variants: [String]) {
        self.question = question
        self.currentAnswer = currentAnswer
        self.correctAnswer = correctAnswer
        self.isCorrect = isCorrect
        self.variants = variants
    }
    
}


import UIKit

protocol GoBackDelegate {
    func goBack()
}
extension Dictionary {
    subscript(i:Int) -> (key:Key,value:Value) {
        get {
            return self[index(startIndex, offsetBy: i)];
        }
    }
}
class SecondViewController: UIViewController, GoBackDelegate {

    @IBOutlet weak var labelQ: UILabel!
    
    var questions = [Question(question: "What is the fundamental rhythmic characteristic of rock ’n’ roll?", currentAnswer: "", correctAnswer: "the dead beat", isCorrect: false, variants: ["the back beat", "the easy beat", "the slow beat", "the dead beat"]),
                     
                     Question(question: "What was the world’s first rock opera?", currentAnswer: "", correctAnswer: "Tommy", isCorrect: false, variants: ["Tommy", "Hair", "Spartacus", "The Phantom of the Opera"]),
                     
                     Question(question: "What is the lead instrument in most rock ’n’ roll bands?", currentAnswer: "", correctAnswer: "Guitar", isCorrect: false, variants: ["Bass", "Guitar", "Keyboard", "Drums"]),
                     
                     Question(question: "What British rock band pioneered the use of the light show?", currentAnswer: "", correctAnswer: "Pink Floyd", isCorrect: false, variants: ["Led Zeppelin", "The Beatles", "Pink Floyd", "The Who"]),
                     
                     Question(question: "What was the first rock song to become famous around the world?", currentAnswer: "", correctAnswer: "Rock Around the Clock", isCorrect: false, variants: ["Teen Angel", "Hound Dog", "Rock Around the Clock", "Words of Love"]),
                     
                     Question(question: "Who was the first rock-and-roll superstar?", currentAnswer: "", correctAnswer: "Elvis Presley", isCorrect: false, variants: ["Madonna", "Elvis Presley", "Clyde McPhatter", "George Harrison"]),
                    
                     Question(question: "What are the instruments in a classic three-piece rock band?", currentAnswer: "", correctAnswer: "Guitar, bass, drums", isCorrect: false, variants: ["Guitar, violin, drums", "Guitar, organ, drums", "Guitar, bass, trumpet", "Guitar, bass, drums"]),
                     
                     
    ]
    
    
    var currentQuestion: Int = 0
    var totalScore: Int = 0
    var randomQuestions: Array<Question> = [] 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        randomQuestions = Quiz.sharedInstance.generate(questions: questions)
        NewQuestion()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
        randomQuestions = Quiz.sharedInstance.generate(questions: questions)
        NewQuestion()
    }
    
    func NewQuestion() {
        labelQ.text = randomQuestions[currentQuestion].question
        let variants = randomQuestions[currentQuestion].variants
        for i in 0..<variants.count {
            let button = UIButton(frame: CGRect(x: 0, y: 52*i+150, width: Int(UIScreen.main.bounds.size.width), height: 50))
            button.backgroundColor = UIColor.darkText
            button.titleLabel?.font = UIFont(name: "Avenir Book", size: 20)
            button.setTitle(variants[i], for: UIControlState.normal)
            button.addTarget(self, action:#selector(buttonClicked(_:)), for: .touchUpInside)
            self.view.addSubview(button)
        }

    }
    
    
    func buttonClicked(_ sender:UIButton) {
        randomQuestions[currentQuestion].currentAnswer = sender.currentTitle!
        if(sender.currentTitle == randomQuestions[currentQuestion].correctAnswer){
            totalScore+=1
            randomQuestions[currentQuestion].isCorrect = true
            super.loadView()
        }
        else {
            randomQuestions[currentQuestion].isCorrect = false
            super.loadView()
        }
        if currentQuestion < randomQuestions.count-1 {
            currentQuestion+=1
            NewQuestion()
        }
        else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "third") as! TableViewController
            vc.delegate  = self
            vc.inite(answers: randomQuestions, totalOfScore: totalScore)
            reset()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func reset() {
        currentQuestion = 0
        totalScore = 0
        randomQuestions = Quiz.sharedInstance.generate(questions: questions)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

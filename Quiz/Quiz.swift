
import UIKit

class Quiz {
    static let sharedInstance = Quiz()
    
    func generate(questions: Array<Question>) -> Array<Question> {
        var temp: Array<Question> = []
        var allNumbers: Array<Int> = []
        for _ in 0..<5 {
            var notExist: Bool = true
            while notExist {
                let random = randomNumber()
                if !allNumbers.contains(random) {
                    allNumbers.append(random)
                    temp.append(questions[random] as Question)
                    notExist = false
                }
            }
        }
        return temp
    }
    
    func randomNumber(range: ClosedRange<Int> = 0...6) -> Int {
        let min = range.lowerBound
        let max = range.upperBound
        return Int(arc4random_uniform(UInt32(1 + max - min))) + min
    }
    
    init() {
    }
}
